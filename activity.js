// 1. What directive is used by Node.js in loading the modules it needs?
const http = require("http");


// 2. What Node.js module contains a method for server creation?
const server = http.createServer((request, response) => {}


// 3. What is the method of the http object responsible for creating a server using Node.js?
const server = http.createServer((request, response) => {}


// 4. What method of the response object allows us to set status codes and content types?
response.writeHead(200, {"Content-Type" : "text/plain"})


// 5. Where will console.log() output its contents when run in Node.js?
console.log(`Server now accessible at localhost: ${port}`);


// 6. What property of the request object contains the address's endpoint?
response.end("Hello Again!");
